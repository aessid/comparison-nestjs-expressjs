import express from "express";
import Calculation from "../models/Calculation";

const router = express.Router();

/* GET home page. */
router.get("/calculations", async function (req, res) {
  const results = await Calculation.findAll();
  res.send({
    title: "GET /calculations",
    results,
  });
});

router.delete("/calculations", async function (req, res) {
  await Calculation.destroy({
    where: {},
    truncate: true,
  });
  res.send({
    title: "DELETE /calculations"
  });
});

router.post(
  "/calculations/calculate/:inputNumber",
  function (req, res) {
    const inputNumber = +req.params.inputNumber;
    let counter = +inputNumber;

    let result = 0;
    while (--counter) {
      result += 2 ** inputNumber;
    }
    const newCalculation = Calculation.build({ result });
    newCalculation.save();

    res.send({
      title: `POST /calculate/${inputNumber}`,
      result: newCalculation,
    });
  }
);

export default router;
