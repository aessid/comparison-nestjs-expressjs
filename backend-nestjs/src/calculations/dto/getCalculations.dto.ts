import CalculationDTO from "./calculation.dto";

type GetCalculationsDto = {
    title: string
    results: CalculationDTO[]
}
export default GetCalculationsDto
