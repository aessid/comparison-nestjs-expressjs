# Comparison of Web-Frameworks "NestJS" and "ExpressJS" for Node.js

Target Audience: Developers

## Setup

To find out how the two technologies compare to each other under the same circumstances, the following description defines the scope of each
application. The goal is to get an understanding on how these apps are structured, integrated and how they perform.
The latest versions will be used for comparison, respectively being [ExpressJS](https://expressjs.com/) `4.17.2` and [NestJS](https://nestjs.com/) `8.2.6.`

### Sample App Requirements

The applications provide the following endpoints:

1. one to fetch all calculations `GET:/calculations` stored in a DB,
2. calculation endpoint `POST:/calculations/calculate/<inputInteger>` to execute a calculation and store it, and
3. an endpoint `DELETE:/calculations` to delete all entries in the database. 

The database will be [PostgreSQL](https://www.postgresql.org/), to see how database interaction is handled. 
[Sequelize](https://sequelize.org/) will be the [ORM](https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping).
[TypeScript](https://www.typescriptlang.org/) will be used as the language to inspect the provided TypeScript support.

The goal is to get an impression on how...

1) each application is structured
2) they integrate with different DBs
3) the frameworks perform under stress

[Node](https://nodejs.org/en/) version 17.5.0 and [npm](https://www.npmjs.com/) version 8.4.1 will be used.

## Starting locally

The `./backend-expressjs/` app runs on port 3000, the database provided by docker-compose at 6660.
The `./backend-nestjs/` app runs on port 4000, the database provided by docker-compose at 7770.
The `start.sh` scripts in each of the folders will start both, DB and app.

## The API Tests

The API test executes multiple scenarios, executing an amount N HTTP requests against the `POST:/calculations/calculate` endpoint with
an input number varying between scenarios. 

It records the total amount of time needed until all requests are resolved, and finally fetches the `GET:/calculations` endpoint
to document how many requests were actually stored in the DB after one second of timeout.

API test scenarios are stored at `./tests/`. The output of the test runs are written to a .json file in the subfolder `testruns`.

## Outcome

In each section below there'll be info on the following aspects:

1) basic technology data
2) app structures, methodologies, overview, overhead
3) TypeScript support and integration
4) basic request performance by throwing different requests at the application (see `./tests/` folder)

Due to time reasons, trade-offs were made in test-creation and deep-dives into existing addons regarding aspects like 
OAuth2 integration, monitoring and error handling.

### ExpressJS

Project data:

> ExpressJS latest stable version: `4.17.2`
>
> Self declaration: "Fast, unopinionated, minimalist web framework for Node.js "
>
> Downloads per week: ~23.5 million
>
> License: MIT
>
> Unpacked Size: 210kB
>
> 56k stars on [GitHub](https://github.com/expressjs/express); 5,669 commits; first commit 26-06-2009
>
> [https://expressjs.com/](https://expressjs.com/)

The app `./backend-express` has been set up using [express-generator](https://www.npmjs.com/package/express-generator) and the `--no-view` parameter.
ExpressJS gives you all the freedom to construct your app in a variety of ways, not enforcing any specific structures.
The self-declared "unopinionated, minimalist web framework" very much fits here, which can be good and bad. 
Working out an app structure, maintaining good coding practices and discipline is mandatory early on, otherwise it may grow uncontrollably. 
TypeScript isn't being provided out of the box, but it's easy to integrate it. After switching, the types support works well.

To create a webapp with the self-set requirements and use-cases mentioned above has been possible in about 2 hours.
I split the application code into few files, which are very short when it comes to line count, totalling about 197 lines.
Further optimizations have not been made due to the time limit.

All endpoints are written in callback-style manner. Handler one or more functions can be assigned to different routes.
```TypeScript
router.get("/calculations", async function (req: any, res: any, next: any) {
  const results = await Calculation.findAll();
  res.send({
    title: "GET /calculations",
    results,
  });
});
```

Since ExpressJS has been around for 13 years now, the ecosystem is well-developed. There are lots of packages taking care
of e.g. prometheus monitoring, OAuth2 integration, etc. And if, for whatever reason, one of the packages doesn't exist, 
it's easy to just extend ExpressJS yourself via your own middleware.

### NestJS

Project data:

> NestJS latest stable version: `8.2.6`
> 
> Self declaration: "A progressive Node.js framework for building efficient and scalable server-side applications."
> 
> Downloads per week: ~1.1 million
> 
> License: MIT
> 
> Unpacked Size: 402kB
> 
> 44.4k stars on [GitHub](https://github.com/nestjs/nest); 10,010 commits; first commit 08-01-2017
> 
> [https://nestjs.com/](https://nestjs.com/)

The NestJS app in this repository has been initialized via the TypeScript starter project at [https://github.com/nestjs/typescript-starter.git](https://github.com/nestjs/typescript-starter.git).
The developers note that TypeScript support has been kept in mind from the very beginning. This can be confirmed, as TS works very well with it.

NestJS actually uses ExpressJS as the underlying platform by default. To achieve higher performance, it is suggested to use
[fastify](https://docs.nestjs.com/techniques/performance#performance-fastify) as an alternative framework, although this very
comparison sticks to ExpressJS.

Unlike ExpressJS, NestJS is opinionated. This means that this framework provided a single way to structure the app.
This comes at a higher cost of overhead during the setup. An app is built using
`Modules`, which is a grouping of `Controllers` and `Services`. This reminds of other WebApp-Frameworks like
Javas [Spring](https://spring.io/) or C# [ASP.NET](https://dotnet.microsoft.com/en-us/apps/aspnet). 
NestJS also includes built-in dependency injection and makes heavy use of
[decorators](https://github.com/tc39/proposal-decorators). As a result, the App structure is not as straight forward as plain ExpressJS.

```TypeScript
// calculations.controller.ts

@Controller('calculations')
export class CalculationsController {
    constructor(private readonly calculationService: CalculationsService) {}

    @Get()
    async getAllCalculations(): Promise<GetCalculationsDto> {
        const results = await this.calculationService.findAllCalculations();
        return {
            title: 'GET /calculations',
            results
        }
    }
    [...]
}
// calculations.service.ts

@Injectable()
export class CalculationsService {
    constructor(
        @InjectModel(Calculation)
        private calculationModel: typeof Calculation,
    ) {}

    async findAllCalculations(): Promise<Calculation[]> {
        return await this.calculationModel.findAll()
    }
    [...]
}

```

While I needed to directly add the dotenv and sequelize package to the ExpressJS app, NestJS
provides already existing Modules `@nestjs/config` and `@nestjs/sequelize` that easily integrate the mentioned packages into
the app.

### Test execution

The following tables shows the test result of a single local execution.
Since these were local test executions, response times will be higher in an actual deployments.
They shall only give an impression on how the apps behave. Further test-optimizations like multiple runs
to get median results is required to have actual comparable data.

Since the `backend-nestjs` also uses ExpressJS as it's underlying platform, these may only give a hint of a potential overhead
that comes from the NestJS setup.

**ExpressJS**

| Input Number | Amount of Calls | Time to execute all POST calculations (ms) | Time to GET all calculations (ms) | Amount of calculations stored |
|---------------|-----------|-----------|---    |---    |
| 10            | 10        | 30        | 10    | 10    |
| 10            | 100       | 164       | 16    | 100   |
| 10            | 1000      | 961       | 31    | 1000  |
| 10            | 10000     | 6616      | 2681  | 9997  |
| 50            | 50     | 30      | 10  | 50  |
| 50            | 100     | 74      | 3  | 100  |
| 50            | 200     | 128      | 19  | 200  |
| 50            | 400     | 254      | 24  | 400  |
| 50            | 800     | 484      | 26  | 800  |
| 50            | 1600     | 968      | 55  | 1600  |
| 50            | 3200     | 2031      | 99  | 3197  |
| 50            | 6400     | 4946      | 1315  | 6396  |
| 50            | 12800     | 12087      | 5199  | 12797  |

**NestJS**

| Input Number | Amount of Calls | Time to execute all POST calculations (ms) | Time to GET all calculations (ms) | Amount of calculations stored |
|---------------|-----------|-----------|---    |---    |
| 10            | 10        | 26        | 6    | 10    |
| 10            | 100       | 154       | 16    | 100   |
| 10            | 1000      | 1002       | 18    | 1000  |
| 10            | 10000     | 5696      | 2509  | 9997  |
| 50            | 50     | 28      | 7  | 50  |
| 50            | 100     | 83      | 10  | 100  |
| 50            | 200     | 136      | 17  | 200  |
| 50            | 400     | 271      | 23  | 400  |
| 50            | 800     | 509      | 36  | 800  |
| 50            | 1600     | 926      | 47  | 1600  |
| 50            | 3200     | 1667      | 131  | 3197  |
| 50            | 6400     | 3311      | 1307  | 6396  |
| 50            | 12800     | 8343      | 4008  | 12797  |

### Conclusion

Both frameworks are good technologies to construct a backend service. 

For APIs with limited scope (simple API), ExpressJS may be the way to go because of lesser overhead.
More ambitious projects may be better off with NestJS, given its rather common approach to development, meaning
controllers, services, models.

There are plenty third-party packages available for both frameworks.

An actual performance difference could not be found in the *very* barbaric test-setup.
