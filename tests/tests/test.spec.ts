import { performance } from "node:perf_hooks";
import axios from "axios";
import * as fs from "fs";

const appURL = process.env.APP_URL || `http://localhost:3000`;
const getCalculationsURL = `${appURL}/calculations`;
const deleteCalculationsURL = `${appURL}/calculations`;
const calculateURL = (input: number) => `${appURL}/calculations/calculate/${input}`;
console.log(`Calling appURL ${appURL}`)

const executeCalculationRequest = async (input: number) =>
  axios.post(calculateURL(input));
const executeCleanupRequest = async () => axios.delete(deleteCalculationsURL);
const executeGetCalculationsRequest = async () => axios.get(getCalculationsURL);

jest.setTimeout(60000);

const callCalculateEndpoint = async (
  arrayOfIntegersUpToCallAmount: number[],
  inputNumberForAPI: number
) => {
  const allCalls = arrayOfIntegersUpToCallAmount.map(() =>
    executeCalculationRequest(inputNumberForAPI)
  );
  return await Promise.all(allCalls);
};

const scenarios = [
  { inputNumber: 10, amountOfCalls: 10 },
  { inputNumber: 10, amountOfCalls: 100 },
  { inputNumber: 10, amountOfCalls: 1000 },
  { inputNumber: 10, amountOfCalls: 10000 },
  { inputNumber: 50, amountOfCalls: 50 },
  { inputNumber: 50, amountOfCalls: 100 },
  { inputNumber: 50, amountOfCalls: 200 },
  { inputNumber: 50, amountOfCalls: 400 },
  { inputNumber: 50, amountOfCalls: 800 },
  { inputNumber: 50, amountOfCalls: 1600 },
  { inputNumber: 50, amountOfCalls: 3200 },
  { inputNumber: 50, amountOfCalls: 6400 },
  { inputNumber: 50, amountOfCalls: 12800 },
];

const allRunOutcomes = [];

describe("Calling the API endpoints", () => {
  afterEach(async () => {
    await executeCleanupRequest();
  });

  afterAll(async () => {
    const fileName = `testruns/testrun_${Date.now()}.json`;
    fs.writeFileSync(fileName, JSON.stringify(allRunOutcomes), "utf-8");
    console.log(`Written test run results to file "${fileName}"`);
  });

  scenarios.forEach((scenario) => {
    const testName = `sending ${scenario.amountOfCalls} requests with inputNumber ${scenario.inputNumber}; calculate and fetch all results`;
    it(testName, async () => {
      console.log(testName);
      const arrayOfIntegersUpToCallAmount = Array.from(
        Array(scenario.amountOfCalls).keys()
      );
      const perfStartCalculations = performance.now();
      try {
        await callCalculateEndpoint(
          arrayOfIntegersUpToCallAmount,
          scenario.inputNumber
        );
      } catch (e) {
        // TODO check why test cases don't fail
        console.error("Found an issue in test case", testName, e);
        throw e
      }
      const perfEndCalculations = performance.now();
      const calculateAllRequestsResultTimeInMS =
        perfEndCalculations - perfStartCalculations;
      console.log(
        `Calling all endpoints took ${calculateAllRequestsResultTimeInMS}ms`
      );

      await new Promise(resolve => setTimeout(resolve, 1000));

      const perfStartGetAllCalculations = performance.now();
      const resultCalculations = await executeGetCalculationsRequest();
      const perfEndGetAllCalculations = performance.now();
      const getAllCalculationsResultTimeInMS =
        perfEndGetAllCalculations - perfStartGetAllCalculations;

      console.log(
        `Calling the GET /calculations endpoint to receive all calculations took ${getAllCalculationsResultTimeInMS}ms`
      );

      allRunOutcomes.push({
        testName,
        scenario,
        calculateAllRequestsResultTimeInMS,
        getAllCalculationsResultTimeInMS,
        amountsOfCalculationsStored: resultCalculations.data.results.length
      });
    });
  });
});
