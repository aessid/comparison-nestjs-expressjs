
import { Column, Model, Table } from 'sequelize-typescript';
import {DataTypes} from "sequelize";

@Table
export class Calculation extends Model {
    @Column({primaryKey: true, autoIncrement: true})
    id: number;

    @Column({type: DataTypes.DOUBLE})
    result: number;

    createdAt: string

    updatedAt: string
}
