import CalculationDTO from "./calculation.dto";

type ExecuteCalculationDTO = {
    title: string
    result: CalculationDTO
}
export default ExecuteCalculationDTO
