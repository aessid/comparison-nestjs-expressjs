import dotenv from "dotenv";
dotenv.config();

export default {
  PG_CONNECTION: `postgres://${process.env.PG_USER}:${process.env.PG_PASSWORD}@${process.env.PG_HOST}:${process.env.PG_PORT}/${process.env.PG_DB}`,
};
