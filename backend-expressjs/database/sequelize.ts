import { Sequelize } from "sequelize";
import config from "../config/config";

const sequelize = new Sequelize(config.PG_CONNECTION);
sequelize.authenticate();

export default sequelize;
