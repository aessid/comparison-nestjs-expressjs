import {Controller, Delete, Get, Param, Post} from '@nestjs/common';
import {CalculationsService} from "./calculations.service";
import GetCalculationsDto from "./dto/getCalculations.dto";
import ExecuteCalculationDTO from "./dto/executeCalculation.dto";

@Controller('calculations')
export class CalculationsController {
    constructor(private readonly calculationService: CalculationsService) {}

    @Get()
    async getAllCalculations(): Promise<GetCalculationsDto> {
        const results = await this.calculationService.findAllCalculations();
        return {
            title: 'GET /calculations',
            results
        }
    }

    @Delete()
    async deleteAllCalculations(): Promise<{ title: string }> {
        await this.calculationService.deleteAllCalculations();
        return {
            title: 'DELETE /calculations'
        }
    }

    @Post('/calculate/:inputNumber')
    async calculate(@Param('inputNumber') inputNumber): Promise<ExecuteCalculationDTO> {
        const calculation = await this.calculationService.performeAndStoreCalculation(inputNumber)
        return {
            title: `POST /calculations/calculate/${inputNumber}`,
            result: calculation
        }
    }
}

