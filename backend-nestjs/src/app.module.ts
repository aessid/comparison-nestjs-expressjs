import {Module} from '@nestjs/common';
import {CalculationsService} from './calculations/calculations.service';
import {CalculationsController} from "./calculations/calculations.controller";
import {SequelizeModule} from '@nestjs/sequelize';
import {ConfigModule, ConfigService} from '@nestjs/config';
import config from "./config/config";
import {Calculation} from "./calculations/model/calculation.model";

@Module({
    imports: [
        ConfigModule.forRoot({
            load: [config],
            isGlobal: true
        }),
        SequelizeModule.forRootAsync({
            inject: [ConfigService],
            useFactory: (config: ConfigService) => ({
                dialect: 'postgres',
                host: config.get<string>('PG_HOST'),
                port: config.get<number>('PG_PORT'),
                username: config.get<string>('PG_USER'),
                password: config.get<string>('PG_PASSWORD'),
                database: config.get<string>('PG_DB'),
                models: [Calculation],
                autoLoadModels: true,
                synchronize: true,
                logging: true
            }),
        }),
        SequelizeModule.forFeature([Calculation])
    ],
    controllers: [CalculationsController],
    providers: [CalculationsService],
})
export class AppModule {
}
