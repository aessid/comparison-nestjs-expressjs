import { DataTypes, Model, Sequelize } from "sequelize";
import sequelize from "../database/sequelize";

class Calculation extends Model {}

Calculation.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    result: {
      type: DataTypes.DOUBLE,
    },
  },
  { sequelize }
);

const sync = async () => {
  try {
    await Calculation.sync({ alter: true });
  } catch (e) {
    console.error(e);
  }
  console.log("sync Calculation");
};
sync().catch((r) => console.log(r));

export default Calculation;
