type CalculationDTO = {
    id: number
    result: number
    createdAt: string
    updatedAt: string
}

export default CalculationDTO
