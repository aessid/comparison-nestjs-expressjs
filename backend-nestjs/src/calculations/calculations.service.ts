import {Injectable} from '@nestjs/common';
import {Calculation} from "./model/calculation.model";
import {InjectModel} from "@nestjs/sequelize";

@Injectable()
export class CalculationsService {
    constructor(
        @InjectModel(Calculation)
        private calculationModel: typeof Calculation,
    ) {}

    async findAllCalculations(): Promise<Calculation[]> {
        return await this.calculationModel.findAll()
    }

    async deleteAllCalculations(): Promise<void> {
        await this.calculationModel.destroy({
            where: {},
            truncate: true,
        });
    }

    async performeAndStoreCalculation(inputNumber: number): Promise<Calculation> {
        let counter = inputNumber;
        let result = 0;
        while (--counter) {
            result += 2 ** inputNumber;
        }
        const newCalculation = this.calculationModel.build({ result })
        newCalculation.save()
        return newCalculation
    }
}
